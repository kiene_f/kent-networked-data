#include "mbed.h"
#include "easy-connect.h"
#include "http_request.h"
#include "LM75B.h"
#include "C12832.h"
#include "DHT.h"
#include <sstream>
#include <string>

Serial host(USBTX, USBRX);
// temperature
LM75B sensor(D14,D15);
// Lcd
C12832 display(D11, D13, D12, D7, D10);

AnalogIn sens(A1);


void dump_response(HttpResponse* res) {
    host.printf("Status: %d - %s\n", res->get_status_code(), res->get_status_message().c_str());

    host.printf("Headers:\n");
    for (size_t ix = 0; ix < res->get_headers_length(); ix++) {
        host.printf("\t%s: %s\n", res->get_headers_fields()[ix]->c_str(), res->get_headers_values()[ix]->c_str());
    }
    host.printf("\nBody (%d bytes):\n\n%s\n", res->get_body_length(), res->get_body_as_string().c_str());
}

int main() {
    std::string dp = "1";
    display.cls();
    display.locate(50,12);
    display.printf("Hello :)");
    
    // Connect to the network (see mbed_app.json for the connectivity method used)
    NetworkInterface *network = easy_connect(true);
    if (!network) {
        display.cls();
        display.locate(1,12);
        display.printf("Cannot connect to the network");
        return 1;
    }

    // Do a GET request to httpbin.org
    //{
        // By default the body is automatically parsed and stored in a buffer, this is memory heavy.
        // To receive chunked response, pass in a callback as last parameter to the constructor.
        /*HttpRequest* get_req = new HttpRequest(network, HTTP_GET, "http://httpbin.org/status/418");

        HttpResponse* get_res = get_req->send();
        if (!get_res) {
            printf("HttpRequest failed (error code %d)\n", get_req->get_error());
            return 1;
        }

        printf("\n----- HTTP GET response -----\n");
        dump_response(get_res);

        delete get_req;
    }*/

    while (1)
    {
        double temp = sensor.temp();
        double light = sens.read();
        light = (1023-light)*10/light - 12200;
        if (dp == "1")
        {
            display.cls();
            display.locate(30,12);
            display.printf("Temp = %.2f", temp);   
            display.locate(30,1);
            display.printf("light = %.2f", light);
        }
        else
            display.cls();
        
        HttpRequest* get_req = new HttpRequest(network, HTTP_GET, "http://192.168.0.1/station/display.php");
        HttpResponse* get_res = get_req->send();
    
        dp = get_res->get_body_as_string().c_str();
        delete get_req;
        
        HttpRequest* post_req = new HttpRequest(network, HTTP_POST, "http://192.168.0.1/station/temperature.php");
        post_req->set_header("Content-Type", "application/json");
    
        stringstream ss;
        ss << "{\"temperature\":\"" << temp << "\"}";
        char *body = (char*)malloc(sizeof(char) * 1024);
        snprintf(body, 1024, "{\"temperature\":\"%.2f\",\"light\":\"%.2f\"}", temp, light);
        host.printf("%s", body);
        HttpResponse* post_res = post_req->send(body, strlen(body));
        if (!post_res) {
            host.printf("(error code %d)\n", post_req->get_error());
        }
        else
        {
        host.printf("\n----- HTTP POST response -----\n");
        dump_response(post_res);
        }
        free(body);
        delete post_req;
        
        wait(2);
    }
}
