var tmp = false;
$(".temperature").click(function() {
	if (tmp == false)
	{
		
		//$(".temperature").width("340px");
		$(".water").animate({ width: "toggle" });
		$(".temperature").animate({ width: "360px" }, function() {
			$(".temp_widget").animate({ width: "toggle" }, function() {
						$(".temp_graph").animate({ width: "toggle" });
			});
		});
		tmp = true;
	}
	else
	{
		$(".temp_graph").animate({ width: "toggle" }, function() {
			$(".temperature").animate({ width: "180px" }, function() {
						$(".temp_widget").animate({ width: "toggle" }, function() {
							$(".water").animate({ width: "toggle" });
						});
			});
		});
		tmp = false;
	}
});

var display = true;
$(".display").click(function() {
	if (display == true)
	{
		$.post( "http://localhost/station/display.php", {display: "0"} );
		$(".display .val").html("OFF");
	}
	else
	{
		$.post( "http://localhost/station/display.php", {display: "1"} );
		$(".display .val").html("ON");
	}
	display = !display;
});
setInterval(function() {
      $.getJSON( "http://localhost/station/temperature.php", function( data ) {
      	$(".temp_widget .val").html(data.temperature[data.temperature.length - 1].value);
      });
      $.getJSON( "http://localhost/station/light.php", function( data ) {
      	$(".temp_light_widget .val").html(data.light[data.light.length - 1].value);
      });
    }, 1000); 

d = new Date();
days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
day = d.getDate();
w = days[d.getDay()];
month = months[d.getMonth()];
h = d.getHours();
y = d.getFullYear();
m = d.getMinutes();
ampm = h >= 12 ? 'pm' : 'am';
h = h % 12;
h = h ? h : 12; // the hour '0' should be '12'
m = m < 10 ? '0' + m : m;
var strTime = h + ':' + m + ' ' + ampm;
document.getElementsByClassName('time')[0].innerHTML = day + ' ' + month + ' ' + y + '<br />' + w + ' ' + strTime;

var body = document.body;
setTimeout(function(){
  body.classList.add('active');
}, 200);