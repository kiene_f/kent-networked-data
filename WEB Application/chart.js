window.onload = function () {

	var dps = []; // dataPoints

	var chart = new CanvasJS.Chart("chartContainer",{
		title :{
			text: "Temperature"
		},			
		data: [{
			type: "area",
			xValueType: "dateTime",
			dataPoints: dps 
		}]
	});

	var xVal = 10;
	var yVal = 30;	
	var updateInterval = 200;
	var dataLength = 10; // number of dataPoints visible at any point
	var last = -1;

	var updateChart = function (count) {
		count = count || 1;
		// count is number of times loop runs to generate random dataPoints.
		
		$.getJSON( "http://localhost/station/temperature.php", function( data ) {
			if (last == - 1)
				last = data.temperature.length - 1 - dataLength;
			var j = 0;
			for (var k = last; k < data.temperature.length; k++) {
				var tmp = data.temperature[k];
				console.log(tmp.value);
				dps.push({
					x: new Date(tmp.time),
					y: parseFloat(tmp.value)
				});
				last = k + 1;
				j++;
			};
		});
		if (dps.length > dataLength)
		{
			dps.shift();				
		}
		
		chart.render();		

	};

	// generates first set of dataPoints
	updateChart(dataLength); 

	// update chart after specified time. 
	setInterval(function(){updateChart()}, updateInterval); 

}