<?php

$json_input_data=json_decode(file_get_contents('php://input'),TRUE);
if (count($json_input_data) > 0)
	addTemperature();
else if (isset($_GET))
	displayTemperature();



function addTemperature()
{
	$json_input_data=json_decode(file_get_contents('php://input'),TRUE);
	$bdd = mysqli_connect('127.0.0.1', 'root', '', 'station');

	if (!$bdd) {
	    printf("Connexion error : %s\n", mysqli_connect_error());
	    exit();
	}

	$temp = strval(floatval ($json_input_data["temperature"]));
	$date = strval(date('Y-m-d H:i:s'));

	echo $temp." ".$date;
	$req_pre = mysqli_prepare($bdd, "INSERT INTO temperature (value, time) VALUES (?, ?)");
	mysqli_stmt_bind_param($req_pre, "ss", $temp, $date);
	mysqli_stmt_execute($req_pre);
	mysqli_stmt_close($req_pre);

	$temp = strval(floatval ($json_input_data["light"]));
	$date = strval(date('Y-m-d H:i:s'));

	echo $temp." ".$date;
	$req_pre = mysqli_prepare($bdd, "INSERT INTO light (value, time) VALUES (?, ?)");
	mysqli_stmt_bind_param($req_pre, "ss", $temp, $date);
	mysqli_stmt_execute($req_pre);
	mysqli_stmt_close($req_pre);

	$req_pre = mysqli_prepare($bdd, "DELETE FROM light WHERE id <= ( SELECT id FROM ( SELECT id FROM light ORDER BY id DESC LIMIT 1 OFFSET 50 ) foo )");
	mysqli_stmt_execute($req_pre);
	mysqli_stmt_close($req_pre);


	mysqli_close($bdd);
}

function displayTemperature()
{
	$bdd = mysqli_connect('127.0.0.1', 'root', '', 'station');

	if (!$bdd) {
	    printf("Connexion error : %s\n", mysqli_connect_error());
	    exit();
	}

	if ($result = mysqli_query($bdd, 'SELECT * FROM temperature ORDER BY time'))
	{
		while($r = mysqli_fetch_assoc($result))
		{
			$temperature[] = $r;
		}
	}
	mysqli_free_result($result);

	echo json_encode(['temperature'=>$temperature]);

	mysqli_close($bdd);
}

?>
