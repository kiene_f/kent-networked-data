# Kent - Networked Data - MY HOME #
### French
L'application web MY HOME permet de suivre en temps réel la température et la luminosité de sa maison en utilisant les capteurs du dispositif FRDM-K64F. Il est même possible de voir l'évolution de la température en temps réel sur un graphique. De plus, il est possible de gérer l'affichage du FRDM à distance (allumer et éteindre). (CO838 INTERNET OF THINGS AND MOBILE DEVICES)
![alt text](http://francois.kiene.fr/assets/images/works/myhome.jpg)

## Built With

* C++
* Jquery
* PHP
* MySQL